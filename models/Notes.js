const {Schema, model} = require('mongoose');

const Notes = new Schema({
  text: {type: String, required: true},
  userId: {type: String, required: true},
  completed: {type: Boolean, required: true},
  createdDate: {type: String, required: true},
});

module.exports = model('Notes', Notes);
