const express = require('express');
const mongoose = require('mongoose');
const authRouter = require('./routes/authRouter');
const notesRouter = require('./routes/notesRouter');
const logger = require('express-simple-logger');
const cors = require('cors');
const dotenv = require('dotenv');


dotenv.config();

const PORT = process.env.PORT;
const database = process.env.MONGOLAB_URI;

const app = express();
app.use(logger());
app.use(cors());
app.use(express.json());
app.use('/api', authRouter, notesRouter);


const start = async () => {
  try {
    await mongoose.connect(database).then(() => console.log('mongo connect'));
    app.listen(PORT, () => console.log(`server started on port ${PORT}`));
  } catch (error) {
    console.log(error);
  }
};

start();
