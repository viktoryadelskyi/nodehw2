const User = require('../models/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');

dotenv.config();

const secret = process.env.SECRET_KEY;

const generateAccessToken = (id, username) => {
  const payload = {
    id,
    username,
  };
  return jwt.sign(payload, secret, {expiresIn: '24h'});
};

class AuthController {
  async registration(req, res) {
    try {
      const {username, password} = req.body;
      const candidate = await User.findOne({
        username,
      });
      if (candidate) {
        return res.status(400).json({message: 'user already exist'});
      }
      const hashPassword = bcrypt.hashSync(password, 10);
      const user = new User({
        username: username,
        password: hashPassword,
        createdDate: new Date().toISOString(),
      });
      await user.save();
      return res.status(200).json({message: 'Success'});
    } catch (e) {
      res.status(500).json({message: 'Registration error'});
    }
  }

  async login(req, res) {
    try {
      const {username, password} = req.body;
      const user = await User.findOne({username});
      if (!user) {
        return res.status(400).json({message: `user ${username}  not found`});
      }
      const validPassword = bcrypt.compareSync(password, user.password);
      if (!validPassword) {
        return res.status(400).json({message: `Password incorrect`});
      }
      const token = generateAccessToken(user._id, username);
      user.token = token;
      return res.json({message: 'Success', jwt_token: token});
    } catch (e) {
      res.status(500).json({message: 'Login error'});
    }
  }

  async getUsers(req, res) {
    try {
      const token = req.headers.authorization;
      const id = jwt.decode(token).id;
      const user = await User.findById(id);
      if (!user) {
        return res.status(400).json({message: 'User not found'});
      }
      const {username, createdDate} = user;

      res.json({
        user: {
          _id: id,
          username,
          createdDate,
        },
      });
    } catch (e) {
      res.status(500).json({message: 'Error'});
    }
  }

  async deleteUser(req, res) {
    try {
      const token = req.headers.authorization;
      const id = jwt.decode(token).id;
      const user = await User.findById(id);
      if (!user) {
        return res.status(400).json({message: 'user doesn\'t exist'});
      }
      user.collection.deleteOne(user);
      res.status(200).json({message: 'Success'});
    } catch (e) {
      res.status(500).json({message: 'Error'});
    }
  }

  async patchPassword(req, res) {
    try {
      const {oldPassword, newPassword} = req.body;
      const token = req.headers.authorization;
      const id = jwt.decode(token).id;
      const user = await User.findById(id);
      const validPassword = bcrypt.compareSync(oldPassword, user.password);

      if (!validPassword) {
        return res.status(400).json({message: 'the password is wrong'});
      }
      const hashPassword = bcrypt.hashSync(newPassword, 10);
      const filter = user;
      const update = {$set: {password: hashPassword}};

      user.collection.findOneAndUpdate(filter, update, {upsert: true});
      res.status(200).json({message: 'Success'});
    } catch (e) {
      res.status(500).json({message: 'Error'});
    }
  }
}

module.exports = new AuthController();
