const jwt = require('jsonwebtoken');
const Notes = require('../models/Notes');


class NotesController {
  async createNotes(req, res) {
    try {
      const {text} = req.body;
      if (!text) {
        return res.status(400).json({message: 'Text is empty'});
      }
      const token = req.headers.authorization;
      if (!token) {
        return res.status(400).json({message: 'Sign in first pls'});
      }
      const userId = jwt.decode(token).id;
      const notes = new Notes({
        text,
        userId,
        completed: false,
        createdDate: new Date().toISOString(),
      });
      await notes.save();
      return res.status(200).json({message: 'Success'});
    } catch (e) {
      res.status(500).json({message: 'Error'});
    }
  }

  async getNotes(req, res) {
    try {
      const token = req.headers.authorization;
      if (!token) {
        return res.status(400).json({message: 'Sign in first pls'});
      }
      const {offset, limit} = req.query;
      const userId = jwt.decode(token).id;
      const notes = await Notes.find({userId});

      const offsetRes = offset || 0;
      const limitRes = limit || 0;

      let resNotes = notes.map((note) => {
        const {_id, text, completed, createdDate} = note;
        return {_id, userId, completed, text, createdDate};
      });

      if (offsetRes) {
        resNotes = resNotes.slice(offsetRes);
      }
      if (limitRes) {
        resNotes = resNotes.slice(
          resNotes.length > limitRes ? resNotes.length - limitRes : 0,
        );
      }
      return res.status(200).json({
        offset: Number(offsetRes),
        limit: Number(limitRes),
        count: resNotes.length,
        notes: resNotes,
      });
    } catch (e) {
      res.status(500).json({message: 'Error'});
    }
  }

  async getNotesById(req, res) {
    try {
      const token = req.headers.authorization;
      const userId = jwt.decode(token).id;
      if (!userId) {
        return res.status(400).json({message: 'Sign in first pls'});
      }
      const {_id, text, completed, createdDate} = await Notes.findOne(
          {userId, _id: req.params.id},
      );

      return res.status(200).json(
          {note: {_id, userId, completed, text, createdDate}},
      );
    } catch (error) {
      return res.status(400).json({message: 'Server error'});
    }
  }

  async putNoteById(req, res) {
    try {
      const {text} = req.body;
      const token = req.headers.authorization;
      const userId = jwt.decode(token).id;
      if (!userId) {
        return res.status(400).json({message: 'Sign in first pls'});
      }
      const note = await Notes.findOne({userId, _id: req.params.id});
      const {completed, createdDate} = note;
      await note.replaceOne({userId, completed, createdDate, text});

      return res.status(200).json({message: 'Success'});
    } catch (error) {
      return res.status(400).json({message: 'Server error'});
    }
  }

  async patchNoteById(req, res) {
    try {
      const token = req.headers.authorization;
      const userId = jwt.decode(token).id;
      if (!userId) {
        return res.status(400).json({message: 'Sign in first pls'});
      }
      const note = await Notes.findOne({userId, _id: req.params.id});
      const reverce = !note.completed;
      const filter = note;
      const update = {$set: {completed: reverce}};

      note.collection.findOneAndUpdate(filter, update, {upsert: true});

      return res.status(200).json({message: 'Success'});
    } catch (error) {
      return res.status(400).json({message: 'Server error'});
    }
  }

  async deleteNoteById(req, res) {
    try {
      const token = req.headers.authorization;
      const userId = jwt.decode(token).id;
      if (!userId) {
        return res.status(400).json({message: 'Sign in first pls'});
      }
      const note = await Notes.findOne({userId, _id: req.params.id});
      if (!note._id) {
        return res.status(400).json({message: 'note doesn\'t exist'});
      }
      note.deleteOne(note);
      return res.status(200).json({message: 'Success'});
    } catch (error) {
      return res.status(400).json({message: 'Server error'});
    }
  }
}

module.exports = new NotesController();
