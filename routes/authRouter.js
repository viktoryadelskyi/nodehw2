const Router = require('express');
const router = new Router();
const controller = require('../controllers/AuthController');

router.post('/auth/register', controller.registration);
router.post('/auth/login', controller.login);
router.get('/users/me', controller.getUsers);
router.delete('/users/me', controller.deleteUser);
router.patch('/users/me', controller.patchPassword);

module.exports = router;
