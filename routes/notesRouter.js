const Router = require('express');
const router = new Router();
const controller = require('../controllers/NotesController');

router.post('/notes', controller.createNotes);
router.get('/notes', controller.getNotes);
router.get('/notes/:id', controller.getNotesById);
router.put('/notes/:id', controller.putNoteById);
router.patch('/notes/:id', controller.patchNoteById);
router.delete('/notes/:id', controller.deleteNoteById);

module.exports = router;
